from .db_migration import query
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer

def get_data(max_reviews=220000):
    print("retrieving data from db... ", end='')
    reviews = query('review', 'is_positive', limit=max_reviews)
    print("done!")
    return reviews

def vectorize(text, vectorizer='tfidf'):
    if vectorizer == 'tfidf':
        cv = TfidfVectorizer(max_features=1000, stop_words='english', lowercase=True)
    if vectorizer == 'count':
        cv = CountVectorizer(max_features=1000, stop_words='english', lowercase=True)

    print("vectorizing review text... ", end='')
    vectorized_text = cv.fit_transform(text)
    print("done!")
    return vectorized_text