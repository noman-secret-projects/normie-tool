from sqlalchemy import create_engine
from sys import exit
import pandas as pd

engine = create_engine('mysql+pymysql://admin:root@localhost/BDSE_a1_scraping')

def query(*column_names, limit):
    # check if callback only had one parameter and happens to be '*'
    if column_names[0] == '*' and len(column_names) == 1:
        cn_str = "*" # get everything
    else:
        cn_str = ', '.join(column_names)

    connection = engine.raw_connection()
    cursor = connection.cursor()
    # call the stored procedure with the INputs
    cursor.callproc("reviews", [cn_str, str(limit)])
    column_names_list = [x[0] for x in cursor.description]
    # setup reviews with column_names
    reviews = [dict(zip(column_names_list, row)) for row in cursor.fetchall()]
    # turn reviews JSON array to pandas Dataframe
    reviews = pd.DataFrame(reviews, columns=column_names_list)
    # close the cursor of the pool.
    cursor.close()
    # commit the connection.
    connection.commit()

    return reviews

def df_to_db(df):
    if type(df) is not None:
        try:
            print("\nPlease wait. Migrating data to database...")

            df.to_sql(name='hotel_reviews', con=engine, if_exists='replace', index=False, chunksize=20000) 

            print("Success! The csv data has been migrated to the database.")

        except Exception as e:
            print(e)
    else:
        exit()