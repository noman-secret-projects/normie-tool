from pathlib import Path
import pandas as pd
from web_scraper.post_scraping import scraped_data

def scrape_process():
    hotel_reviews_data = scraped_data()

    hotel_reviews_labels = [
        'Hotel_Address', 'Additional_Number_of_Scoring', 'Review_Date', 'Average_Score', 'Hotel_Name', 'Reviewer_Nationality', 'Negative_Review', 'Review_Total_Negative_Word_Counts', 
        'Total_Number_of_Reviews', 'Positive_Review', 'Review_Total_Positive_Word_Counts', 'Total_Number_of_Reviews_Reviewer_Has_Given', 'Reviewer_Score', 'Tags', 'days_since_review', 'lat', 'lng'
    ]

    hotel_reviews_df = pd.DataFrame(data=hotel_reviews_data, columns=hotel_reviews_labels)

    csv_path = (Path(__file__).parent.parent / "../datasets/Scraped_Hotel_Reviews.csv").resolve()

    hotel_reviews_df.to_csv(csv_path, index=False)

    print("\nCreated csv file in datasets folder.")

def combine_dataframes():
    kaggle_path = (Path(__file__).parent.parent / "../datasets/Kaggle_Hotel_Reviews.csv").resolve()
    hand_written_path = (Path(__file__).parent.parent / "../datasets/Hand_Written_Hotel_Reviews.csv").resolve()
    scraped_path = (Path(__file__).parent.parent / "../datasets/Scraped_Hotel_Reviews.csv").resolve()

    if kaggle_path.is_file() and hand_written_path.is_file() and scraped_path.is_file():
        
        kaggle_df = pd.read_csv(kaggle_path, sep=',')
        scraped_df = pd.read_csv(scraped_path, sep=',')
        hand_written_df = pd.read_csv(hand_written_path, sep=',')

        frames = [kaggle_df, scraped_df, hand_written_df]
        # combine dataframes into one
        df = pd.concat(frames, sort=False)
        df.reset_index(drop=True, inplace=True)

        return df

    else:
        print("dataset folder misses a csv file, either scrape it or create it.")
        return None
