from .data_csv import combine_dataframes, scrape_process
from .db_migration import df_to_db
from cleaner.clean import clean
from .generate_wordcloud import generate_wordcloud
from trainer.trainer_menu import trainer_menu
from trainer.predictor_menu import predictor_menu
from sys import exit

def default_menu():
    print("\nWhat would you like to do (type in the number to continue):")
    print("1. Scrape data from booking.com.")
    print("2. Migrate csv data to database.")
    print("3. Train database data.")
    print("4. Predict models.")
    print("5. Generate wordcloud.")
    print("0. Quit Program.")
    choice = input("> ")

    if "1" == choice:
        scrape_process()
        default_menu()

    if "2" == choice:
        df = combine_dataframes()
        cleaned_df = clean(df)
        df_to_db(cleaned_df)
        exit()

    if "3" == choice:
        trainer_menu()

    if "4" == choice:
        predictor_menu()

    if "5" == choice:
        generate_wordcloud()

    if "0" == choice:
        exit()

    else:
        exit()
