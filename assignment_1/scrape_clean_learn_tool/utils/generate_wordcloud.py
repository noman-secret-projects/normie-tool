from pathlib import Path
import numpy as np
import pandas as pd
from os import path
from PIL import Image
from .db_migration import query
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
import matplotlib.pyplot as plt

def generate_wordcloud():
    df = query("*", limit=1)

    print("There are {} observations and {} features in this dataset. \n".format(df.shape[0],df.shape[1]))

    print("There are {} countries containing reviews in this dataset such as {}... \n".format(len(df['Reviewer_Nationality'].unique()), ", ".join(df['Reviewer_Nationality'].unique()[0:5])))
    text = df.review[0]

    wordcloud = WordCloud(max_font_size=50, max_words=100, background_color="white").generate(text)
    img_path = (Path(__file__).parent.parent / "img/wordcloud.png").resolve()
    wordcloud.to_file(img_path)
    print("saved wordcloud to disk.")