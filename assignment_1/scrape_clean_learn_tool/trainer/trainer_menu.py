from .models.svm import predict_svm, train_svm
from .models.lsvm import predict_lsvm, train_lsvm
from .models.logistic_regression import predict_lr, train_lr
from .models.naive_bayes import predict_nb, train_nb

def trainer_menu():
    print("\nHow would you like to train the data (type in the number to continue):")
    print("1. Train with SVC.")
    print("2. Train with Linear SVC.")
    print("3. Train with Linear Regression.")
    print("4. Train with Naive Bayes.")
    print("0. Quit program.")
    choice = input("> ")

    if "1" == choice:
        train_svm()
        predict_svm()
        exit()

    if "2" == choice:
        train_lsvm()
        predict_lsvm()
        exit()

    if "3" == choice:
        train_lr()
        predict_lr()
        exit()

    if "4" == choice:
        train_nb()
        predict_nb()
        exit()


    if "0" == choice:
        exit()
    else:
        exit()