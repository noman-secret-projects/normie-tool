from pathlib import Path
from sklearn.metrics import plot_roc_curve
import matplotlib.pyplot as plt
from joblib import load
from utils.trainer_helpers import vectorize, get_data
from sklearn.model_selection import train_test_split


def predict_all():
    svm_path = (Path(__file__).parent / "models_output/trained_model_svm.joblib").resolve()
    lsvm_path = (Path(__file__).parent / "models_output/trained_model_lsvm.joblib").resolve()

    lr_path = (Path(__file__).parent / "models_output/trained_model_lr.joblib").resolve()
    nb_path = (Path(__file__).parent / "models_output/trained_model_nb.joblib").resolve()

    svc = load(svm_path)
    lsvc = load(lsvm_path)
    lr = load(lr_path)
    nb = load(nb_path)

    reviews = get_data(max_reviews=210000)

    # reviews_for_lr = get_data(max_reviews=400000)
    # reviews_for_svc = get_data()

    vectorized_text = vectorize(reviews['review'])

    # vectorized_text_for_lr = vectorize(reviews_for_lr['review'], vectorizer='count')
    # vectorized_text_for_svc = vectorize(reviews_for_svc['review'], vectorizer='tfidf')
    
    # print("splitting X/y data into test/train data for logistic regression... ", end='')
    # X_train_lr, X_test_lr , y_train_lr, y_test_lr = train_test_split(vectorized_text_for_lr, reviews_for_lr['is_positive'], train_size=0.8, random_state=42)
    # print("done!")
    print("splitting X/y data into test/train data... ", end='')
    X_train, X_test , y_train, y_test = train_test_split(vectorized_text, reviews['is_positive'], train_size=0.8, random_state=42)
    print("done!")
    # print("splitting X/y data into test/train data for SVC... ", end='')
    # X_train_svc, X_test_svc , y_train_svc, y_test_svc = train_test_split(vectorized_text_for_svc, reviews_for_svc['is_positive'], train_size=0.8, random_state=42)
    # print("done!")

    ax = plt.gca()
    # plot_roc_curve(svc, X_test_svc, y_test_svc, ax=ax)
    # plot_roc_curve(lr, X_test_lr, y_test_lr, ax=ax)
    clfs = [lsvc, svc, lr, nb]

    for i in clfs:
        plot_roc_curve(i, X_test, y_test, ax=ax)

    plt.show()