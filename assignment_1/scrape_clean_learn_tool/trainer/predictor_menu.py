from .predict_all_models import predict_all
from .models.svm import predict_svm
from .models.lsvm import predict_lsvm
from .models.naive_bayes import predict_nb
from .models.logistic_regression import predict_lr

def predictor_menu():
    print("\nWhich model would you like to predict (type in the number to continue):")
    print("1. Predict with SVC classifier.")
    print("2. Predict with Linear SVC classifier.")
    print("3. Predict with Linear Regression classifier.")
    print("4. Predict with Bernoulli Naive Bayes classifier.")
    print("5. Predict all.")
    print("0. Quit program.")
    choice = input("> ")

    if "1" == choice:
        predict_svm()
        exit()
    
    if "2" == choice:
        predict_lsvm()
        exit()

    if "3" == choice:
        predict_lr()
        exit()

    if "4" == choice:
        predict_nb()
        exit()

    if "5" == choice:
        predict_all()
        exit()

    if "0" == choice:
        exit()
    else:
        exit()