from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score, plot_roc_curve, confusion_matrix, average_precision_score, accuracy_score
from utils.trainer_helpers import get_data, vectorize
from joblib import dump, load


def train_svm():
    svc = svm.SVC(kernel='linear')

    reviews = get_data()

    vectorized_text = vectorize(reviews['review'], vectorizer='tfidf')

    print("splitting X/y data into test/train data... ", end='')
    X_train, X_test , y_train, y_test = train_test_split(vectorized_text, reviews['is_positive'], train_size=0.8, random_state=42)
    print("done!")

    print("fitting the train data... ", end='')
    svc.fit(X_train, y_train)
    print("done!")

    svm_path = (Path(__file__).parent.parent / "models_output/trained_model_svm.joblib").resolve()

    print("Saving model... ", end='')
    dump(svc, svm_path)
    print("done!")

def predict_svm():
    svm_path = (Path(__file__).parent.parent / "models_output/trained_model_svm.joblib").resolve()
    svc = load(svm_path)

    reviews = get_data()

    vectorized_text = vectorize(reviews['review'])

    print("splitting X/y data into test/train data... ", end='')
    X_train, X_test , y_train, y_test = train_test_split(vectorized_text, reviews['is_positive'], train_size=0.8, random_state=42)
    print("done!")

    y_pred = svc.predict(X_test)
    y_score = svc.decision_function(X_test)

    c_matrix = pd.DataFrame(confusion_matrix(y_test, y_pred), index = ['P: Negative', 'P: Positive'], columns = ['Actual: Negative', 'Actual: Positive'])
    print(c_matrix)

    print("\nFinal Accuracy: %s" % accuracy_score(y_test, y_pred))
    print("F1 Score: {0:0.2f}".format(average_precision_score(y_test, y_score)))
    print("AUC Score: %s" % roc_auc_score(y_test, y_score))

    plot_roc_curve(svc, X_test, y_test)
    plt.show()