from sklearn.naive_bayes import BernoulliNB
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from utils.trainer_helpers import vectorize, get_data
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix, roc_auc_score, plot_roc_curve
from joblib import dump, load
from pathlib import Path

def train_nb():
    bnb = BernoulliNB()

    reviews = get_data(max_reviews=210000)
    vectorized_text = vectorize(reviews['review'])

    print("splitting X/y data into test/train data... ", end='')
    X_train, X_test , y_train, y_test = train_test_split(vectorized_text, reviews['is_positive'], train_size=0.8, random_state=42)
    print("done!")

    print("fitting the train data... ", end='')
    model = bnb.partial_fit(X_train.toarray(), y_train, np.unique(y_train))
    print("Done!")

    nb_path = (Path(__file__).parent.parent / "models_output/trained_model_nb.joblib").resolve()

    print("Saving model... ", end='')
    dump(model, nb_path)
    print("done!")

def predict_nb():
    nb_path = (Path(__file__).parent.parent / "models_output/trained_model_nb.joblib").resolve()
    bnb = load(nb_path)

    reviews = get_data(max_reviews=210000)

    vectorized_text = vectorize(reviews['review'], vectorizer='count')
    
    print("splitting X/y data into test/train data... ", end='')
    X_train, X_test , y_train, y_test = train_test_split(vectorized_text, reviews['is_positive'], train_size=0.8, random_state=42)
    print("done!")
    
    y_pred = bnb.predict(X_test.toarray())

    confused_matrix = pd.DataFrame(confusion_matrix(y_pred, y_test), index = ['P: Negative', 'P: Positive'], columns = ['Actual: Negative', 'Actual: Positive'])
    print(confused_matrix)

    print("\nFinal Accuracy: %s" % accuracy_score(y_test, y_pred))
    print("AUC Score: %s" % roc_auc_score(y_test, bnb.predict_proba(X_test.toarray())[::,1]))

    plot_roc_curve(bnb, X_test, y_test)
    plt.show()