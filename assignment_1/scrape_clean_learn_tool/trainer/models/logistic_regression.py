import pandas as pd
import matplotlib.pyplot as plt
from utils.trainer_helpers import vectorize, get_data
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix, roc_auc_score, plot_roc_curve, average_precision_score
from joblib import dump, load
from pathlib import Path


def train_lr():
    lr = LogisticRegression(C=0.7, random_state=42, max_iter=700)
    reviews = get_data(max_reviews=400000)

    vectorized_text = vectorize(reviews['review'], vectorizer='count')

    print("splitting X/y data into test/train data... ", end='')
    X_train, X_test , y_train, y_test = train_test_split(vectorized_text, reviews['is_positive'], train_size=0.8, random_state=42)
    print("done!")

    print("fitting the train data... ", end='')
    lr.fit(X_train, y_train)
    print("Done!")

    lr_path = (Path(__file__).parent.parent / "models_output/trained_model_lr.joblib").resolve()

    print("Saving model... ", end='')
    dump(lr, lr_path)
    print("done!")

def predict_lr():
    lr_path = (Path(__file__).parent.parent / "models_output/trained_model_lr.joblib").resolve()
    lr = load(lr_path)

    reviews = get_data(max_reviews=400000)

    vectorized_text = vectorize(reviews['review'], vectorizer='count')
    
    print("splitting X/y data into test/train data... ", end='')
    X_train, X_test , y_train, y_test = train_test_split(vectorized_text, reviews['is_positive'], train_size=0.8, random_state=42)
    print("done!")

    y_pred = lr.predict(X_test)
    y_score = lr.decision_function(X_test)

    c_matrix = pd.DataFrame(confusion_matrix(y_test, y_pred), index = ['P: Negative', 'P: Positive'], columns = ['Actual: Negative', 'Actual: Positive'])
    print(c_matrix)

    print("\nFinal Accuracy: %s" % accuracy_score(y_test, y_pred))
    print("F1 Score: {0:0.2f}".format(average_precision_score(y_test, y_score)))
    print("AUC Score: %s" % roc_auc_score(y_test, lr.predict_proba(X_test)[::,1]))

    plot_roc_curve(lr, X_test, y_test)
    plt.show()