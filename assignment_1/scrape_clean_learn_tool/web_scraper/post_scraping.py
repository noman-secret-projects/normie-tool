from web_scraper.pre_data_fetching import clean_urls
from web_scraper.scraping import scrape_page, scraped_objects
import time
from selenium import webdriver

page_urls = clean_urls()

chrome_options = webdriver.ChromeOptions()
chrome_options.headless = True
        
def scraped_data():
    for url in page_urls:
        browser = webdriver.Chrome() # options=chrome_options
        browser.get(url)
        # get unique metadata do determine this is the right website.
        site = browser.find_element_by_xpath("//meta[@property='og:site_name']")

        if "Booking.com" in site.get_attribute("content"):
            
            # before we are going to scrape, let's delete that annoying "Accept cookies" banner on the page
            remove_consent_banner_js = "let el = document.getElementById(\"onetrust-consent-sdk\");el.remove();"
            time.sleep(1)
            browser.execute_script(remove_consent_banner_js)

            scrape_page(browser)

        else:
            # TODO: substring url so that it just shows the name of the domain.
            print(str(url) + " is not a booking.com website page!")    

        browser.quit()

    # return scraped objects
    return scraped_objects()