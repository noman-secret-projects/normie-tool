import time
from datetime import datetime
import re

from selenium.webdriver.common.by import By
import selenium.webdriver.support.expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from utils.progressbar import printProgressBar

objects = []

def scrape_page(browser):
    # hotel name
    title = browser.find_element_by_id("hp_hotel_name").text.strip()
    # hotel address
    address = browser.find_element_by_class_name("hp_address_subtitle").text.strip()
    address.replace(',', ' ')
    # lat/lng coordinates    
    latlng = browser.find_element_by_id("hotel_header").get_attribute("data-atlas-latlng").split(",")
    lat = latlng[0]
    lng = latlng[-1]
    # average score of hotel
    avg_score = browser.find_element_by_xpath("//div[@id='js--hp-gallery-scorecard']/a/div/div[@class='bui-review-score__badge']").text.strip()
    total_reviews = browser.find_element_by_xpath("//div[@id='js--hp-gallery-scorecard']/a/div/div[@class='bui-review-score__content']/div[@class='bui-review-score__text']").text.strip()
    total_reviews = total_reviews.split(" ")[0].replace(",", "")
    # show all reviews in the side panel
    browser.find_element_by_xpath("//a[@rel='reviews']").click()
    # show dropdown first...
    browser.find_element_by_xpath("//div[@id='review_lang_filter']").click()
    # select english in dropdown, to filter on english only reviews
    browser.find_element_by_xpath("//div[@id='review_lang_filter']/div[@class='bui-dropdown__content']/div[@class='bui-dropdown-menu']/ul[@class='bui-dropdown-menu__items']/li[2]").click()
    # wait 10 seconds on browser for reviews to load in...
    WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "div#review_list_page_container>ul")))
    # get every review in the container
    reviews = browser.find_elements_by_xpath("//ul[@class='review_list']/*")

    time.sleep(1)
    element = browser.find_element_by_xpath("//div[@class='bui-pagination__pages']/div[@class='bui-pagination__list']")
    browser.execute_script("arguments[0].scrollIntoView();", element)

    pages = browser.find_element_by_xpath("//div[@class='bui-pagination__pages']/div[@class='bui-pagination__list']/div[last()]/a[@class='bui-pagination__link']/span[1]")

    total_pages = list(range(2, (int(pages.text) + 1)))
    len_pages = len(total_pages)

    print("\nHotel: {hotel_name}".format(hotel_name=title))
    print("URL: {url}".format(url=browser.current_url))
    print("Total amount of reviews: {total_reviews} reviews".format(total_reviews=total_reviews))
    print("Number of pages: {pages}".format(pages=len_pages + 1))

    printProgressBar(0, len_pages, prefix = 'Progress:', suffix = 'Complete', length = 50)

    for i, page in enumerate(total_pages):
        time.sleep(1)
        # wait 10 seconds on browser for reviews to load in...
        WebDriverWait(browser, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "div#review_list_page_container>ul")))
        # get every review in the container
        reviews = browser.find_elements_by_xpath("//ul[@class='review_list']/*")
        
        for review in reviews:

            non_valid_review_content = review.find_elements_by_css_selector("div.bui-grid>div.c-review-block__right div.c-review-block__row")
            if len(non_valid_review_content) == 1:
                break

            nationality = None
            additional_number_of_scoring = None
            total_num_of_reviews_reviewer_given = None
            room_info = 'n/a'
            reviewer_score = None
            positive_review = 'No Positive'
            negative_review = 'No Negative'
            total_words_positive_review = 0
            total_words_negative_review = 0

            review_rows = review.find_elements_by_css_selector("div.bui-grid>div.c-review-block__right>div.c-review-block__row>div.c-review>div.c-review__row")

            for row in review_rows:
                if "lalala" not in row.get_attribute("class"):
                    # check if there is a positive review available
                    spans = row.find_elements_by_css_selector("p.c-review__inner span")

                    if len(spans) == 1:
                        positive_review = "No Positive"
                        negative_review = "No Negative"
                        break
                    else:
                        positive_review = row.find_element_by_css_selector("p> span.c-review__body").text.strip().replace("\n", " ")
                        if "NA" == positive_review:
                            positive_review = 'No Positive'

                else:
                    negative_review = row.find_element_by_css_selector("p> span.c-review__body").text.strip().replace("\n", " ")
                    if "NA" == negative_review:
                        negative_review = 'No Negative'

            if "No Positive" != positive_review:
                total_words_positive_review = len(re.split('; |, | |\*|\n',positive_review))

            if "No Negative" != negative_review:
                total_words_negative_review = len(re.split('; |, | |\*|\n',negative_review))

            stay_info = review.find_elements_by_css_selector("div.bui-grid>div.c-review-block__left div.c-review-block__row")

            for div in stay_info:
                if "room-info" in div.get_attribute("class"):
                    room_info = div.find_element_by_css_selector("ul>li>a>div.bui-list__body").text.strip()
                    break
                else:
                    room_info = "n/a"

            stay_duration = review.find_element_by_css_selector("div.bui-grid>div.c-review-block__left>ul.c-review-block__stay-date>li>div.bui-list__body").text
            stay_duration = stay_duration.split("·")[0].strip()            
            visitor_type = review.find_element_by_css_selector("div.bui-grid>div.c-review-block__left>ul.review-panel-wide__traveller_type>li>div.bui-list__body").text.strip()
            tags = [room_info, stay_duration, visitor_type]

            guest_profile = review.find_elements_by_css_selector("div.bui-grid>div.c-review-block__left>div.c-review-block__guest>div.c-guest>div.bui-avatar-block__text span")

            for span in guest_profile:
                if "subtitle" in span.get_attribute("class"):
                    nationality = span.text.strip()
          
            reviewer_score = review.find_element_by_css_selector("div.bui-grid>div.c-review-block__right>div.c-review-block__row>div.bui-grid>div.bui-grid__column-2 div.bui-review-score__badge").text.strip()
            
            now = datetime.now()

            time.sleep(0.4)
            date_string = review.find_element_by_css_selector("div.bui-grid>div.c-review-block__right>div.c-review-block__row span.c-review-block__date").text.split(": ")[-1]
            date_time = datetime.strptime(date_string, '%d %B %Y')
            review_date = date_time.strftime("%d/%m/%Y").lstrip("0").replace(" 0", " ")
            
            delta = now - date_time
            days_since_review = str(delta.days) + " days"

            objects.append([
                address, additional_number_of_scoring, review_date, float(avg_score), 
                title, nationality, negative_review, int(total_words_negative_review),
                int(total_reviews), positive_review, int(total_words_positive_review),
                total_num_of_reviews_reviewer_given, float(reviewer_score), tags, days_since_review, lat, lng
            ])
            # append everything here

        time.sleep(0.4)
        link = browser.find_element_by_xpath("//a[span[text()='{page_num}']]".format(page_num=str(page)))

        time.sleep(0.4)
        element = browser.find_element_by_xpath("//div[@class='bui-pagination__pages']/div[@class='bui-pagination__list']")
        browser.execute_script("arguments[0].scrollIntoView();", element)
        
        link.click()
        time.sleep(0.1)
        printProgressBar(i + 1, len_pages, prefix = 'Progress:', suffix = 'Complete', length = 50)

def scraped_objects():
    return objects