# this process if for removing all newlines in the text file and put them all in the list.
# if you want to run the whole process, run the file [main.py] which is in the same directory as this file.
from pathlib import Path

def clean_urls():
    city_urls = []
    
    txt_path = (Path(__file__).parent / "../web_scraper/data_sources.txt").resolve()

    with open(txt_path) as resource_urls:
        for url in resource_urls:
            city_urls.append(url.replace('\n', ''))
    
    return city_urls