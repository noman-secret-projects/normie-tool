from sys import exit, path
path.append('../')

from utils.default_menu import default_menu


print("Welcome to the Normie Tool!\nIt includes a web scraper, scripts to clean the dataset and learning models.")

print("Before you start scraping, check the following information to let this program work properly.")
print("\t - Have the chrome driver installed properly.")
print("\t - During scraping, the program will open a browser window, do not close it.")
print("\t - Keep in mind that it takes approximately 1 hour to scrape 150 pages.")
print("\t - Breaking the program during scraping has a low chance of creating corrupt csv files.")


acknowledgement =  input("\nBy typing in yes, you acknowledge to have properly checked your system and have checked the information above. type anything else to exit.\n> ")

if "yes" == acknowledgement:
    default_menu()
else:
    exit()