import pandas as pd

def clean(df):
    print("\nCleaning dataset...")
    # Remove columns for Additional_Number_of_Scoring, Total_Number_of_Reviews_Reviewer_Has_Given, lat and lng.
    df.drop(columns=['Additional_Number_of_Scoring', 'Total_Number_of_Reviews_Reviewer_Has_Given', 'lat', 'lng'], axis='columns', inplace=True)

    # Remove rows which miss certain values.
    df.dropna(axis = 'rows', how ='any', inplace=True)

    # pandas complains about making a copy without using the .loc or the .copy()
    df = df[(df['Review_Total_Negative_Word_Counts']>=5) & (df['Review_Total_Positive_Word_Counts']>=5)].copy()

    # Extract the number of days out of the string and keep that.
    df['days_since_review'] = df['days_since_review'].str.split().str[0].astype(int)
    
    # label data
    temp_df_neg = df.loc[:, df.columns != 'Positive_Review']
    temp_df_pos = df.loc[:, df.columns != 'Negative_Review']

    temp_df_neg['is_positive'] = 0
    temp_df_pos['is_positive'] = 1
    
    temp_df_neg.rename({'Negative_Review':'review'}, axis=1, inplace=True)
    temp_df_pos.rename({'Positive_Review':'review'}, axis=1, inplace=True)

    combined_df = pd.concat([temp_df_neg, temp_df_pos], axis=0)

    df = combined_df.sample(frac=1, random_state=42).reset_index(drop=True)
    print("Succesfully cleaned dataset.")
    return df
